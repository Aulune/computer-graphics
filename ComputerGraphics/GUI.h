#pragma once
#include <sdlgui/screen.h>
#include <sdlgui/window.h>
#include <sdlgui/layout.h>
#include <sdlgui/label.h>
#include <sdlgui/checkbox.h>
#include <sdlgui/button.h>
#include <sdlgui/textbox.h>
#include <sdlgui/imageview.h>
#include <sdlgui/tabwidget.h>
#include <sdlgui/formhelper.h>

#include <iostream>

#include "MyGraphicsLibrary.h"

#if defined(_WIN32)
#include <SDL.h>
#include <SDL_image.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#endif

using namespace sdlgui;

class GUI final : public Screen
{
private:
	SDL_Texture* blueprintTexture = nullptr;
	GraphicsLibrary::Astroid* const gr_object;
	GraphicsLibrary::Grid* const grid;
public:
	GUI(SDL_Window* pwindow, int rwidth, int rheight, GraphicsLibrary::Astroid* const gr_object, GraphicsLibrary::Grid* const grid)
		: Screen(pwindow, Vector2i(rwidth, rheight), "Computer graphics labs"),
		gr_object(gr_object), grid(grid)
	{
		this->createImageWidget(pwindow, rwidth, rheight);
		this->createInteractWidget(pwindow, rwidth, rheight);

		Screen::performLayout(mSDL_Renderer);
	}

	~GUI() override
	{
		SDL_DestroyTexture(blueprintTexture);
	}

	void draw(SDL_Renderer* renderer) override
	{
		Screen::draw(renderer);
	}

private:

	void createImageWidget(SDL_Window* const pwindow, const int rwidth, const int rheight)
	{
		blueprintTexture = IMG_LoadTexture(SDL_GetRenderer(pwindow), "resources/images/blueprint.jpg");
		if (blueprintTexture == nullptr)
		{
			throw std::runtime_error("Could not open image data!");
		}

		auto& img_window = window("Blueprint image");
		img_window.setPosition({ rwidth - 450, 0 });
		img_window.withLayout<GroupLayout>();

		auto* imageView = img_window.add<ImageView>(blueprintTexture);

		imageView->setGridThreshold(20);
		imageView->setPixelInfoThreshold(20);
	}

	void createInteractWidget(SDL_Window* const pwindow, const int rwidth, const int rheight)
	{
		auto& window = wdg<Window>("Interact");

		//window.setFixedSize({ 400, 500 });
		window.setLayout(new GroupLayout());
		TabWidget* tabWidget = window.add<TabWidget>();

		Widget* layer = tabWidget->createTab("Sizes");

		auto* layout = new GridLayout(Orientation::Horizontal, 2,
			Alignment::Middle, 15, 5);
		layout->setColAlignment({ Alignment::Maximum, Alignment::Fill });
		layout->setSpacing(0, 10);
		layer->setLayout(layout);

		layer->add<Label>("Grid size :", "sans-bold");
		layer->add<Label>("15px", "sans-bold");

		// Adding Object Parameters
		{
			addParameter(layer, "R", std::to_string(static_cast<int>(gr_object->getRadius())), "px", [&](const std::string& str)->bool
				{
					return gr_object->setRadius(std::stof(str));
				});
			addParameter(layer, "a", std::to_string(static_cast<int>(gr_object->getA())), "", [&](const std::string& str)->bool
				{
					return gr_object->setA(std::stof(str));
				});
			addParameter(layer, "b", std::to_string(static_cast<int>(gr_object->getB())), "", [&](const std::string& str)->bool
				{
					return gr_object->setB(std::stof(str));
				});
		}
		// Adding Euclide transformations
		{
			layer = tabWidget->createTab("Euclide");
			layer->setLayout(layout);

			layer->add<Label>("Move", "sans-bold");
			layer->add<Label>(":", "sans-bold");

			auto& moveX = addParameter(layer, "x", "0", "px");
			auto& moveY = addParameter(layer, "y", "0", "px");

			layer->add<Label>("Rotate", "sans-bold");
			layer->add<Label>(":", "sans-bold");
			auto& rotate = addParameter(layer, "angle", "0", "deg");
			layer->add<Label>("x", "sans-bold");
			auto& rotateX = addField(layer, "0", "px");
			layer->add<Label>("y", "sans-bold");
			auto& rotateY = addField(layer, "0", "px");

			layer->add<Button>("Confirm", [&]()
				{
					GraphicsLibrary::Transform transform;

					transform.move(std::stof(moveX.value()), std::stof(moveY.value()), 0);
					GraphicsLibrary::Vector3f rotation(0, 0, std::stof(rotate.value()));
					GraphicsLibrary::Vector3f center(std::stof(rotateX.value()), std::stof(rotateY.value()), 0);
					transform.rotate(rotation, grid->getZeroCoordinate() + center);

					gr_object->combineTransform(transform);
				});
		}
		
		// Adding Affine transformations
		{
			layer = tabWidget->createTab("Affine");
			layout = new GridLayout(Orientation::Horizontal, 3,
				Alignment::Middle, 15, 5);
			layout->setColAlignment({ Alignment::Maximum, Alignment::Fill });
			layout->setSpacing(0, 10);
			layer->setLayout(layout);

			layer->add<Label>("vector", "sans-bold");
			layer->add<Label>("x", "sans-bold");
			layer->add<Label>("y", "sans-bold");

			layer->add<Label>("r0", "sans-bold");
			auto& r0X = addField(layer, "0", "px");
			auto& r0Y = addField(layer, "0", "px");


			layer->add<Label>("rX", "sans-bold");
			auto& r1X = addField(layer, std::to_string(grid->getCellSize()), "px");
			auto& r1Y = addField(layer, "0", "px");


			layer->add<Label>("rY", "sans-bold");
			auto& r2X = addField(layer, "0", "px");
			auto& r2Y = addField(layer, std::to_string(grid->getCellSize()), "px");


			layer->add<Button>("Confirm", [&]()
				{
					GraphicsLibrary::Transform transform;

					GraphicsLibrary::Vector3f r0(std::stof(r0X.value()) / grid->getCellSize(), std::stof(r0Y.value()) / grid->getCellSize(), 0);
					GraphicsLibrary::Vector3f rX(std::stof(r1X.value()) / grid->getCellSize(), std::stof(r1Y.value()) / grid->getCellSize(), 0);
					GraphicsLibrary::Vector3f rY(std::stof(r2X.value()) / grid->getCellSize(), std::stof(r2Y.value()) / grid->getCellSize(), 0);
					transform.move(-grid->getZeroCoordinate());
					transform.affine(r0, rX, rY);
					transform.move(grid->getZeroCoordinate());

					gr_object->combineTransform(transform);
					grid->combineTransform(transform);
				});
		}
		// Adding Homography transformations
		{
			layer = tabWidget->createTab("Homography");
			layout = new GridLayout(Orientation::Horizontal, 4,
				Alignment::Middle, 15, 5);
			layout->setColAlignment({ Alignment::Maximum, Alignment::Fill });
			layout->setSpacing(0, 10);
			layer->setLayout(layout);

			layer->add<Label>("vector", "sans-bold");
			layer->add<Label>("x", "sans-bold");
			layer->add<Label>("y", "sans-bold");
			layer->add<Label>("w", "sans-bold");

			layer->add<Label>("r0", "sans-bold");
			auto& r0X = addField(layer, "0", "");
			auto& r0Y = addField(layer, "0", "");
			auto& rW0 = addField(layer, "1", "");

			layer->add<Label>("rX", "sans-bold");
			auto& r1X = addField(layer, "1", "");
			auto& r1Y = addField(layer, "0", "");
			auto& rWX = addField(layer, "1", "");

			layer->add<Label>("rY", "sans-bold");
			auto& r2X = addField(layer, "0", "");
			auto& r2Y = addField(layer, "1", "");
			auto& rWY = addField(layer, "1", "");


			layer->add<Button>("Confirm", [&]()
				{
					GraphicsLibrary::Transform transform;

					GraphicsLibrary::Vector3f r0(std::stof(r0X.value()), std::stof(r0Y.value()), 0);
					GraphicsLibrary::Vector3f rX(std::stof(r1X.value()), std::stof(r1Y.value()), 0);
					GraphicsLibrary::Vector3f rY(std::stof(r2X.value()), std::stof(r2Y.value()), 0);
					GraphicsLibrary::Vector3f w(std::stof(rWX.value()), std::stof(rWY.value()), std::stof(rW0.value()));

					transform.move(grid->getZeroCoordinate());
					transform.homography(r0, rX, rY, w);
					transform.move(-grid->getZeroCoordinate());

					gr_object->combineTransform(transform);
					grid->combineTransform(transform);
				});
		}
		tabWidget->setActiveTab(0);
	}
	static TextBox& addField(Widget* const widget, const std::string& defaultValue, const std::string& unitType)
	{
		auto& textBox = widget->wdg<TextBox>();
		textBox.setEditable(true);
		textBox.setFixedSize(Vector2i(100, 20));
		textBox.setUnits(unitType);
		textBox.setValue(defaultValue);
		textBox.setDefaultValue(defaultValue);
		textBox.setFontSize(16);
		textBox.setFormat("-?[0-9]+[.]?[0-9]*");

		return textBox;
	}
	static TextBox& addParameter(Widget* const widget, const std::string& title, const std::string& defaultValue,
		const std::string& unitType)
	{
		widget->add<Label>(title + " :", "sans-bold");
		auto& textBox = widget->wdg<TextBox>();
		textBox.setEditable(true);
		textBox.setFixedSize(Vector2i(100, 20));
		textBox.setUnits(unitType);
		textBox.setValue(defaultValue);
		textBox.setDefaultValue(defaultValue);
		textBox.setFontSize(16);
		textBox.setFormat("-?[0-9]+[.]?[0-9]*");

		return textBox;
	}

	static TextBox& addParameter(Widget* const widget, const std::string& title, const std::string& defaultValue, const std::string& unitType,
		const std::function<bool(const std::string&)>& callback)
	{
		widget->add<Label>(title + " :", "sans-bold");
		auto& textBox = widget->wdg<TextBox>();
		textBox.setEditable(true);
		textBox.setFixedSize(Vector2i(100, 20));
		textBox.setUnits(unitType);
		textBox.setValue(defaultValue);
		textBox.setDefaultValue(defaultValue);
		textBox.setFontSize(16);
		textBox.setFormat("[1-9][0-9]*");

		textBox.setCallback(callback);
		
		return textBox;
	}
};