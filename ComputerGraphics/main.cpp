#include "GUI.h"

#include <iostream>
#include <map>

#include "MyGraphicsLibrary.h"

#if defined(_WIN32)
#include <Windows.h>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#endif

#undef main

std::map<int, TTF_Font*> fonts;

bool SDL_RenderDrawText(SDL_Renderer* renderer, char const* const text, float x, float y,
                        int size, SDL_Color color);

int main() try
{
	SDL_Init(SDL_INIT_VIDEO);   // Initialize SDL2
	TTF_Init();
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 16);

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	const int winWidth = 1280;
	const int winHeight = 900;

	// Create an application window with the following settings:
	SDL_Window* window = SDL_CreateWindow(
		"An SDL2 window", //    const char* title
		SDL_WINDOWPOS_UNDEFINED, //    int x: initial x position
		SDL_WINDOWPOS_UNDEFINED, //    int y: initial y position
		winWidth, //    int w: width, in pixels
		winHeight, //    int h: height, in pixels
		SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_ALLOW_HIGHDPI  //    Uint32 flags: window options, see docs
	);

	// Check that the window was successfully made
	if (window == nullptr) {
		throw std::exception((std::string("Could not create window: ") + SDL_GetError()).c_str());
	}

	SDL_Renderer* renderer = SDL_CreateRenderer(window, SDL_VIDEO_RENDER_OGL, SDL_RENDERER_ACCELERATED);
	SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

	GraphicsLibrary::Brush::getInstance().setDrawPointCallback([&renderer](const float x, const float y)
		{
			SDL_RenderDrawPointF(renderer, x, y);
		});

	GraphicsLibrary::Brush::getInstance().setDrawLineCallback([&renderer](const float x1, const float y1, const float x2, const float y2)
		{
			SDL_RenderDrawLineF(renderer, x1, y1, x2, y2);
		});

	GraphicsLibrary::Brush::getInstance().setChangeColorCallback([&renderer](const uint8_t r, const uint8_t g, const uint8_t b, const uint8_t a)
		{
			SDL_SetRenderDrawColor(renderer, r, g, b, a);
		});

	GraphicsLibrary::Brush::getInstance().setDrawTextCallback([renderer](const char* text, float x, float y, int size, GraphicsLibrary::Color color)->bool
	{
		return SDL_RenderDrawText(renderer, text, x, y, size, { color.r, color.g, color.b, color.a });
	});

	GraphicsLibrary::Grid grid(winWidth, winHeight, 15);

	GraphicsLibrary::Astroid object(grid.getZeroCoordinate(), 90);
	GUI* screen = new GUI(window, winWidth, winHeight, &object, &grid);

	bool quit = false;
	SDL_Event e;

	std::pair<GraphicsLibrary::Line, GraphicsLibrary::Line>* tnn = nullptr;
	
	std::chrono::high_resolution_clock::time_point timePoint = std::chrono::high_resolution_clock::now();
	while (!quit)
	{
		//Handle events on queue
		while (SDL_PollEvent(&e) != 0)
		{
			//User requests quit
			if (e.type == SDL_QUIT)
			{
				quit = true;
			}
			else if (e.type == SDL_WINDOWEVENT && e.window.event == SDL_WINDOWEVENT_RESIZED)
			{
				screen->resizeCallbackEvent(e.window.data1, e.window.data2);
			}
			else if (e.type == SDL_MOUSEBUTTONDOWN)
			{
				delete tnn;

				int x, y;
				SDL_GetMouseState(&x, &y);
				tnn = object.getNormalAndTangent(x, y);
			}
                
                
			screen->onEvent(e);
		}

		const auto deltaTime = std::chrono::duration_cast<std::chrono::duration<float>>(std::chrono::high_resolution_clock::now() - timePoint).count();
		timePoint = std::chrono::high_resolution_clock::now();
		GraphicsLibrary::PointsAnimator::getInstance().update(deltaTime);
		
		SDL_SetRenderDrawColor(renderer, 0xff, 0xff, 0xff, 0xff);
		SDL_RenderClear(renderer);

		// draw figures
		grid.draw();
		GraphicsLibrary::Brush::getInstance().setColor(GraphicsLibrary::Color::Black);
		object.draw();
		if(tnn != nullptr)
		{
			GraphicsLibrary::Brush::getInstance().setColor(GraphicsLibrary::Color::Blue);
			tnn->first.draw();
			GraphicsLibrary::Brush::getInstance().setColor(GraphicsLibrary::Color::Red);
			tnn->second.draw();
		}
		// draw gui
		screen->drawAll();

		// Render the rect to the screen
		SDL_RenderPresent(renderer);
	}
	for (auto& font : fonts)
	{
		TTF_CloseFont(font.second);
	}
	
	delete tnn;
	TTF_Quit();

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}
catch (const std::exception& e)
{
	const auto error_msg = std::string("Caught a fatal error: ") + e.what();
	std::cerr << error_msg << std::endl;
	return -1;
}

bool SDL_RenderDrawText(SDL_Renderer* renderer, char const* const text, const float x, const float y,
                        int size, SDL_Color color)
{
	if(fonts.find(size) == fonts.end())
	{
		auto* const font = TTF_OpenFont("resources/Roboto-Regular.ttf", size);
		
		if (font == nullptr) {
			std::cerr << "error: font not found" << std::endl;
			return false;
		}

		fonts[size] = font;
	}
	
	// create text surface
	SDL_Surface* surface = TTF_RenderText_Solid(fonts[size], text, color);
	// error occured
	if (!surface) return false;

	// create texture from surface for displaying via renderer
	SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
	// error occured
	if (!texture) return false;

	// specifying position and size for texture rendering
	SDL_Rect rect{ static_cast<int>(x), static_cast<int>(y) , surface->w, surface->h };

	// free surface
	SDL_FreeSurface(surface);
	
	// render copy of texture & check if not succeeded
	if (SDL_RenderCopy(renderer, texture, nullptr, &rect) < 0)
		return false;

	// free texture
	SDL_DestroyTexture(texture);
	
	// succeeded
	return true;
}