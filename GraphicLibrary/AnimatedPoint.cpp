﻿#include "AnimatedPoint.h"

namespace GraphicsLibrary
{
	AnimatedPoint::AnimatedPoint(Vector3f* const point)
		: point(point)
	{}

	AnimatedPoint::AnimatedPoint(Vector3f* const point, const Vector3f& endPoint)
		: point(point)
	{
		this->transform(endPoint);
	}

	void AnimatedPoint::transform(const Vector3f& endPoint)
	{
		start = *point;
		end = endPoint;
		offset = end - start;
		
		_isActive = true;
	}

	bool AnimatedPoint::isActive() const
	{
		return _isActive;
	}

	void AnimatedPoint::update(const float deltaTime)
	{
		if (!isActive())
			return;

		const float eps = 1;
		const Vector3f diff = end - *point;
		if (std::abs(diff.x) <= eps
			&& std::abs(diff.y) <= eps
			&& std::abs(diff.z) <= eps)
		{
			_isActive = false;
			return;
		}
		
		*point += offset * deltaTime * speed;
	}
}
