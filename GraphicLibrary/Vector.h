#pragma once
namespace GraphicsLibrary
{
	template <typename T>
	class Vector3
	{
	public:
		/**
		 * \brief X coordinate of the point
		 */
		T x;
		
		/**
		 * \brief Y coordinate of the point
		 */
		T y;

		/**
		 * \brief Z coordinate of the point
		 */
		T z;
		
		Vector3() :x(), y(), z()
		{}

		template<typename U>
		explicit Vector3(const Vector3<U> vector);
		
		Vector3(const T x, const T y, const T z)
			:x(x), y(y), z(z)
		{}
	};

	template <typename T>
	template <typename U>
	Vector3<T>::Vector3(const Vector3<U> vector)
		: x(static_cast<T>(vector.x)),
		  y(static_cast<T>(vector.y)),
		  z(static_cast<T>(vector.z))
	{}

	template<typename T>
	Vector3<T> operator+(const Vector3<T>& a, const Vector3<T>& b)
	{
		return Vector3<T>(a.x + b.x, a.y + b.y, a.z + b.z);
	}

	template<typename T>
	Vector3<T> operator-(const Vector3<T>& a, const Vector3<T>& b)
	{
		return Vector3<T>(a.x - b.x, a.y - b.y, a.z - b.z);
	}
	
	template<typename T>
	Vector3<T> operator-(const Vector3<T>& v)
	{
		return Vector3<T>(-v.x, -v.y, -v.z);
	}

	template<typename T>
	Vector3<T> operator*(const Vector3<T>& a, const T coefficient)
	{
		return Vector3<T>(coefficient*a.x, coefficient*a.y, coefficient*a.z);
	}
	template<typename T>
	Vector3<T> operator*(const T coefficient, const Vector3<T>& a)
	{
		return Vector3<T>(coefficient*a.x, coefficient*a.y, coefficient*a.z);
	}
	
	template <typename T>
	Vector3<T>& operator+=(Vector3<T>& a, const Vector3<T>& b)
	{
		a.x += b.x;
		a.y += b.y;
		a.z += b.z;
		return a;
	}

	template <typename T>
	Vector3<T>& operator-=(Vector3<T>& a, const Vector3<T>& b)
	{
		a.x -= b.x;
		a.y -= b.y;
		a.z -= b.z;
		return a;
	}

	typedef Vector3<int> Vector3i;
	typedef Vector3<float> Vector3f;
}