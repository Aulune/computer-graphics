﻿#pragma once
#include <vector>

#include "Object.h"
#include "Brush.h"

namespace GraphicsLibrary
{
	class Circle final : public Object
	{
	private:
		Vector3f center{};
		
		float radius{};
		
		uint16_t pointCount;

		Transform transform;

		Vector3f* points = nullptr;
		
	public:
		Circle();
		
		Circle(const Vector3f& center, float radius, uint16_t pointCount = 30);

		Circle(const Circle& circle);
		Circle& operator=(const Circle& circle);
		
		Circle(Circle&& circle) noexcept;
		Circle& operator=(Circle&& circle) noexcept;
		
		~Circle() override;

		void setCenter(const Vector3f& center);
		[[nodiscard]] Vector3f getCenter() const;
		
		void setRadius(float radius);
		[[nodiscard]] float getRadius() const;

		void setPointCount(uint16_t pointCount);
		[[nodiscard]] uint16_t getPointCount() const;
		
		void combineTransform(Transform& transform) override;
		
		void draw() const override;

	private:
		void createPoints() const;
		[[nodiscard]] Vector3f getPoint(uint16_t index) const;
	};
}
