﻿#include "Line.h"

namespace GraphicsLibrary
{
	Line::Line(const Vector3f& firstPoint, const Vector3f& secondPoint)
		: firstPoint(firstPoint), secondPoint(secondPoint),
		  transformedFirstPoint(firstPoint), transformedSecondPoint(secondPoint)
	{}

	Vector3f Line::getFirstPoint() const
	{
		return firstPoint;
	}

	void Line::setFirstPoint(const Vector3f& point)
	{
		this->firstPoint = point;
		transformedFirstPoint = transform.multiply(firstPoint);
	}

	void Line::setSecondPoint(const Vector3f& point)
	{
		this->secondPoint = point;
		transformedSecondPoint = transform.multiply(secondPoint);
	}

	Vector3f Line::getSecondPoint() const
	{
		return secondPoint;
	}

	void Line::setPoints(const Vector3f& firstPoint, const Vector3f& secondPoint)
	{
		this->setFirstPoint(firstPoint);
		this->setSecondPoint(secondPoint);
	}

	void Line::combineTransform(Transform& transform)
	{
		this->transform.combine(transform);

		transformedFirstPoint = this->transform.multiply(firstPoint);
		transformedSecondPoint = this->transform.multiply(secondPoint);
	}

	void Line::draw() const
	{
		Brush::getInstance().drawLine(transformedFirstPoint, transformedSecondPoint);
	}
}
