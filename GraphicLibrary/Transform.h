#pragma once
#include <cmath>
#include <iostream>

#include "Vector.h"

namespace GraphicsLibrary
{
	class Transform final
	{
	private:
		float matrix[16]{};
		
	public:
		Transform() noexcept;
		
		Transform(const Transform& transform) noexcept;
		Transform& operator=(const Transform& transform) noexcept;
		
		Transform(Transform&& transform) noexcept;
		Transform& operator=(Transform&& transform) noexcept;
		
		~Transform() = default;
		
		void scale(const Vector3f& scale);
		void move(const Vector3f& vector);
		void rotate(const Vector3f& angle);
		void rotate(const Vector3f& angle, const Vector3f& center);
		
		void scale(float x, float y, float z);
		void move(float x, float y, float z);
		void rotate(float x, float y, float z);

		void affine(const Vector3f& r0, const Vector3f& rX, const Vector3f& rY);
		void homography(const Vector3f& r0, const Vector3f& rX, const Vector3f& rY, const Vector3f& w);
		
		[[nodiscard]] Vector3f multiply(const Vector3f& point) const;
		[[nodiscard]] Vector3f multiply(float x, float y, float z) const;

		Transform& combine(const Transform& transform);
#ifdef _DEBUG
		void print() const;
#endif
	private:
		Transform& combine(float const * const transform);
		void setMatrix(float const * const matrix);
	};
}
