﻿#include "Arc.h"

namespace GraphicsLibrary
{
	Arc::Arc()
	{
		this->setPointCount(30);
	}

	Arc::Arc(const Vector3f& center, float radius,   // NOLINT(cppcoreguidelines-pro-type-member-init)
		float phi1, float phi2,
		uint16_t pointCount)
		: center(center), radius(radius)
	{
		this->setPointCount(pointCount);
		this->setPhi1(phi1);
		this->setPhi2(phi2);
	}

	Arc::Arc(const Arc& arc)	// NOLINT(cppcoreguidelines-pro-type-member-init)
	{
		center = arc.center;
		radius = arc.radius;
		transform = arc.transform;
		phi1 = arc.phi1;
		phi2 = arc.phi2;

		this->setPointCount(arc.pointCount);
	}

	Arc& Arc::operator=(const Arc& arc)
	{
		if (this == &arc) return *this;

		center = arc.center;
		radius = arc.radius;
		transform = arc.transform;
		phi1 = arc.phi1;
		phi2 = arc.phi2;

		this->setPointCount(arc.pointCount);
		return *this;
	}

	Arc::Arc(Arc&& arc) noexcept
	{
		center = arc.center;
		radius = arc.radius;
		transform = std::move(arc.transform);
		pointCount = arc.pointCount;
		points = arc.points;
		phi1 = arc.phi1;
		phi2 = arc.phi2;

		arc.points = nullptr;
	}

	Arc& Arc::operator=(Arc&& arc) noexcept
	{
		if (this == &arc) return *this;

		center = arc.center;
		radius = arc.radius;
		transform = std::move(arc.transform);
		pointCount = arc.pointCount;
		phi1 = arc.phi1;
		phi2 = arc.phi2;

		points = arc.points;
		arc.points = nullptr;

		return *this;
	}

	Arc::~Arc()
	{
		delete[] points;
	}

	void Arc::setCenter(const Vector3f& center)
	{
		this->center = center;
		this->createPoints();
	}

	Vector3f Arc::getCenter() const
	{
		return center;
	}

	void Arc::setRadius(float radius)
	{
		this->radius = radius;
		this->createPoints();
	}

	float Arc::getRadius() const
	{
		return radius;
	}

	void Arc::setPhi1(float phi1)
	{
		const auto PI = 3.14159265358979323846;
		this->phi1 = phi1 * PI / 180.;
		this->createPoints();
	}

	void Arc::setPhi1(const Vector3f& point)
	{
		phi1 = -std::acos((center.x - point.x)/radius);
		this->createPoints();
	}

	void Arc::setPhi2(const Vector3f& point)
	{
		phi2 = -std::acos((center.x-point.x)/radius);
		this->createPoints();
	}

	float Arc::getPhi1() const
	{
		return phi1;
	}

	void Arc::setPhi2(float phi2)
	{
		const auto PI = 3.14159265358979323846;
		this->phi2 = phi2 * PI / 180.;
		this->createPoints();
	}

	float Arc::getPhi2() const
	{
		return phi2;
	}

	void Arc::setPointCount(uint16_t pointCount)
	{
		this->pointCount = pointCount;

		delete[] points;

		points = new Vector3f[static_cast<size_t>(pointCount)];
		this->createPoints();
	}

	uint16_t Arc::getPointCount() const
	{
		return pointCount;
	}

	void Arc::combineTransform(Transform& transform)
	{
		this->transform.combine(transform);
		this->createPoints();
	}

	void Arc::draw() const
	{
		Vector3f secondPoint = points[0];

		for (uint_fast16_t i = 1; i < pointCount; i++)
		{
			const auto firstPoint = secondPoint;
			secondPoint = points[i];
			Brush::getInstance().drawLine(firstPoint, secondPoint);
		}
	}

	void Arc::createPoints() const
	{
		const auto segment = (phi2 - phi1) / static_cast<float>(pointCount - 1);

		auto currentAngle = phi1;
		for (uint_fast16_t index = 0; index < pointCount; index++)
		{
			const auto x = std::cos(currentAngle) * radius + center.x;
			const auto y = std::sin(currentAngle) * radius + center.y;

			points[index] = transform.multiply(x, y, 0);

			currentAngle += segment;
		}
	}
}
