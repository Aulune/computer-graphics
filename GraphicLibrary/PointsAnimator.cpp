﻿#include "PointsAnimator.h"

namespace GraphicsLibrary
{
	void PointsAnimator::animatePoint(Vector3f* const point, const Vector3f& endPoint)
	{
		points.emplace_back(point, endPoint);
	}

	void PointsAnimator::update(const float deltaTime)
	{
		auto it = points.begin();
		while(it != points.end())
		{
			if(it->isActive())
			{
				it->update(deltaTime);
				++it;
			}
			else it = points.erase(it);
		}
	}
}
