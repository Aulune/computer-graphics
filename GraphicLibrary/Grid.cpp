﻿#include "Grid.h"

namespace GraphicsLibrary
{
	Grid::Grid(int width, int height, int cellSize)
		: cellSize(cellSize), axes({
		static_cast<float>(std::ceil(width / cellSize / 2.) * cellSize),
		static_cast<float>(std::ceil(height /cellSize / 2.) * cellSize),
		0},
		width, height)
	{
		lines.resize(width/cellSize + height/cellSize);
		for(auto i = 0; i < width/cellSize; i++)
		{
			lines[i] = new Line(Vector3f(cellSize*i, 0, 0),Vector3f(cellSize * i, height, 0));
		}
		for(auto i = 0; i < height/cellSize; i++)
		{
			lines[i+width/cellSize] = new Line(Vector3f(0, cellSize*i, 0),Vector3f(width, cellSize * i, 0));
		}
	}

	Grid::~Grid()
	{
		for (auto* line : lines)
		{
			delete line;
		}
	}

	int Grid::getCellSize() const
	{
		return cellSize;
	}

	Vector3f Grid::getZeroCoordinate() const
	{
		return axes.getZeroCoordinate();
	}

	void Grid::combineTransform(Transform& transform)
	{
		for (auto* line : lines)
		{
			line->combineTransform(transform);
		}
		axes.combineTransform(transform);
	}

	void Grid::draw() const
	{
		Brush::getInstance().setColor(Color::LightGray);
		for (auto* line : lines)
		{
			line->draw();
		}
		Brush::getInstance().setColor(Color::Black);

		axes.draw();
	}
}
