﻿#pragma once
#include "Arc.h"
#include "Circle.h"
#include "Line.h"
#include "Object.h"

namespace GraphicsLibrary
{
	class MyVariant final : public Object
	{
	private:
		const int cell = 45;
		Vector3f rightPoints[9];
		Vector3f leftPoints[9] = {
			Vector3f(cell, 0, 0),
			Vector3f(3 * cell, 0, 0),
			Vector3f(cell * 3, cell * 2, 0),
			Vector3f(cell * 2, cell * 3, 0),
			Vector3f(cell, cell * 3, 0),
			Vector3f(cell, cell * 2, 0),
			Vector3f(cell * 2, cell * 2, 0),
			Vector3f(cell * 2, cell, 0),
			Vector3f(cell, cell, 0)
		};
		
		Line lines[18];
		Line additionalLines[2];
		
		Arc arcs[2];
		Circle circle;

		Vector3f Z;
		Vector3f O1;
		
	public:
		MyVariant(Vector3f zeroCoordinate);
		void combineTransform(Transform& transform) override;
		void draw() const override;

		bool setR1(float radius);
		[[nodiscard]] float getR1() const;
		
		bool setR2(float radius);
		[[nodiscard]] float getR2() const;

		bool setR3(float radius);
		[[nodiscard]] float getR3() const;

		bool setL1(float size);
		[[nodiscard]] float getL1() const;
		
		bool setL2(float size);
		[[nodiscard]] float getL2() const;

		bool setL3(float size);
		[[nodiscard]] float getL3() const;

		bool setL4(float size);
		[[nodiscard]] float getL4() const;

		bool setL5(float size);
		[[nodiscard]] float getL5() const;

		bool setL6(float size);
		[[nodiscard]] float getL6() const;

		bool setL7(float size);
		[[nodiscard]] float getL7() const;

		bool setL8(float size);
		[[nodiscard]] float getL8() const;

		bool setL9(float size);
		[[nodiscard]] float getL9() const;
		
	private:
		void recreateLines();
		
		void conjugation(const Vector3f& Z, const Vector3f& Q1, float R);
		
		static std::pair<float, float> getLine(const Vector3f& A, const Vector3f& B);
		static Vector3f intersection(const std::pair<float, float>& a, const std::pair<float, float>& b);
	};
}
