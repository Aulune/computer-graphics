﻿#pragma once
#include <optional>
#include "PointsAnimator.h"

#include "Object.h"
#include "Transform.h"
#include "Vector.h"
#include "Brush.h"
#include "Line.h"

namespace GraphicsLibrary
{
	class Astroid final : public Object
	{
	private:
		Vector3f center{};
		
		float radius{};

		float a = 1;
		float b = 1;
		
		uint16_t pointCount;

		Transform transform;

		Vector3f* points = nullptr;
		
	public:
		Astroid();

		explicit Astroid(const Vector3f& center, float radius = 100, uint16_t pointCount = 60);

		Astroid(const Astroid& astroid);
		Astroid& operator=(const Astroid& astroid);
		
		Astroid(Astroid&& astroid) noexcept;
		Astroid& operator=(Astroid&& astroid) noexcept;
		
		~Astroid() override;

		[[nodiscard]] std::pair<Line, Line>* getNormalAndTangent(int x, int y) const;
		
		void setCenter(const Vector3f& center);
		[[nodiscard]] Vector3f getCenter() const;

		bool setRadius(float radius);
		[[nodiscard]] float getRadius() const;

		[[nodiscard]] float getA() const;
		bool setA(float v);
		[[nodiscard]] float getB() const;
		bool setB(float v);
		
		[[nodiscard]] float getArea() const;
		[[nodiscard]] float getRadiusOfCurvature(float t) const;
		[[nodiscard]] float getArcLength() const;
		
		void setPointCount(uint16_t pointCount);
		[[nodiscard]] uint16_t getPointCount() const;
		
		void combineTransform(Transform& transform) override;
		
		void draw() const override;
		
	private:
		void createPoints() const;
		
		void animatedMovePoints() const;
		
		[[nodiscard]] Vector3f getPoint(uint16_t index) const;
	};
}
