﻿#pragma once
#include <functional>

#include "Common.h"

namespace GraphicsLibrary
{
	/**
	 * \brief Singleton decorator
	 */
	class Brush final
	{
	private:
		std::function<void(float, float, float, float)> drawLineCallback;
		std::function<void(float, float)> drawPointCallback;
		
		std::function<void(uint8_t, uint8_t, uint8_t, uint8_t)> changeColorCallback;

		std::function<bool(const char*, float, float, int, Color)> drawTextCallback;
		
		Brush() = default;
		
	public:
		Brush(Brush&& brush) = delete;
		Brush(Brush& brush) = delete;
		void operator=(Brush&&) = delete;
		void operator=(const Brush&) = delete;
		
		~Brush() = default;

		/**
		 * \brief Get instance of singleton
		 * \return Instance of singleton
		 */
		static Brush& getInstance();

		/**
		 * \brief Sets callback for point drawing
		 * \param func Function to draw a line
		 */
		void setDrawPointCallback(std::function<void(float, float)>&& func);
		
		/**
		 * \brief Sets callback for line drawing
		 * \param func Function to draw a line
		 */
		void setDrawLineCallback(std::function<void(float, float, float, float)>&& func);
		
		/**
		 * \brief Sets callback for changing color
		 * \param func Function to change color
		 */
		void setChangeColorCallback(std::function<void(uint8_t, uint8_t, uint8_t, uint8_t)>&& func);

		/**
		 * \brief Draws a point
		 * \param x Coordinate X of point
		 * \param y Coordinate Y of point
		 */
		void drawPoint(float x, float y) const;
		
		/**
		 * \brief Draws a point
		 * \param point Point to draw
		 */
		void drawPoint(Vector3f point) const;
		
		/**
		 * \brief Draws a line by coordinates
		 * \param x1 Coordinate X of first point
		 * \param y1 Coordinate Y of first point
		 * \param x2 Coordinate X of second point
		 * \param y2 Coordinate Y of second point
		 */
		void drawLine(float x1, float y1, float x2, float y2) const;

		/**
		 * \brief Uses callback to draw line by points
		 * \param p1 First point of line
		 * \param p2 Second point of line
		 */
		void drawLine(Vector3f p1, Vector3f p2) const;

		/**
		 * \brief Uses callback to change drawing color
		 * \param r Red component
		 * \param g Green component
		 * \param b Blue component
		 * \param a Alpha (opacity) component
		 */
		void setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a) const;

		/**
		 * \brief Uses callback to change drawing color
		 * \param color Color to be set
		 */
		void setColor(const Color& color) const;

		/**
		 * \brief Sets callback for drawing text
		 * \param func Function to draw text
		 */
		void setDrawTextCallback(std::function<bool(const char*, float, float, int, Color)>&& func);

		/**
		 * \brief Uses callback to draw text
		 * \param text Text to draw
		 * \param x Coordinate X of upper left text's corner
		 * \param y Coordinate Y of upper left text's corner
		 * \param size Font size to draw text
		 * \param color Text color
		 */
		void drawText(const char* text, float x, float y, int size, Color color) const;

		/**
		 * \brief Uses callback to draw text
		 * \param text Text to draw
		 * \param position Position of upper left text's corner
		 * \param size Font size to draw text
		 * \param color Text color
		 */
		void drawText(const char* text, Vector3f position, int size, Color color) const;
	};
}
