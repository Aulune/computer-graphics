#pragma once
#include <cstdint>

namespace GraphicsLibrary
{
	class Color
	{
	public:
		uint8_t r;
		uint8_t g;
		uint8_t b;
		uint8_t a;

		explicit Color(const uint8_t r = 0, const uint8_t g = 0,
			const uint8_t b = 0, const uint8_t a = 255)
			: r(r), g(g), b(b), a(a)
		{}

		static Color Black;
		static Color White;
		static Color LightGray;
		static Color Red;
		static Color Green;
		static Color Blue;
	};
}
