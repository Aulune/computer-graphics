﻿#pragma once
#include "Common.h"
#include "Brush.h"
#include "Object.h"

namespace GraphicsLibrary
{
	class Line final : public Object
	{
	private:
		Vector3f firstPoint{};
		Vector3f secondPoint{};
		
		Vector3f transformedFirstPoint;
		Vector3f transformedSecondPoint;

		Transform transform;
		
	public:
		Line() = default;
		Line(const Vector3f& firstPoint, const Vector3f& secondPoint);

		[[nodiscard]] Vector3f getFirstPoint() const;
		void setFirstPoint(const Vector3f& point);
		[[nodiscard]] Vector3f getSecondPoint() const;
		void setSecondPoint(const Vector3f& point);
		
		void setPoints(const Vector3f& firstPoint, const Vector3f& secondPoint);
		
		void combineTransform(Transform& transform) override;

		void draw() const override;
	};
}
