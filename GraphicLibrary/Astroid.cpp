﻿#include "Astroid.h"

namespace GraphicsLibrary
{
	Astroid::Astroid()  // NOLINT(cppcoreguidelines-pro-type-member-init)
	{
		this->setPointCount(30);
	}

	Astroid::Astroid(const Vector3f& center, float radius, uint16_t pointCount)   // NOLINT(cppcoreguidelines-pro-type-member-init)
		: center(center), radius(radius)
	{
		this->setPointCount(pointCount);
	}

	Astroid::Astroid(const Astroid& astroid)
	{
		center = astroid.center;
		radius = astroid.radius;
		transform = astroid.transform;

		this->setPointCount(astroid.pointCount);
	}

	Astroid& Astroid::operator=(const Astroid& astroid)
	{
		if(this == &astroid) return *this;
		
		center = astroid.center;
		radius = astroid.radius;
		transform = astroid.transform;

		this->setPointCount(astroid.pointCount);
		return *this;
	}

	Astroid::Astroid(Astroid&& astroid) noexcept
	{
		center = astroid.center;
		radius = astroid.radius;
		transform = std::move(astroid.transform);
		pointCount = astroid.pointCount;
		
		points = astroid.points;
		astroid.points = nullptr;
	}

	Astroid& Astroid::operator=(Astroid&& astroid) noexcept
	{
		if(this == &astroid) return *this;
		
		center = astroid.center;
		radius = astroid.radius;
		transform = std::move(astroid.transform);
		pointCount = astroid.pointCount;

		points = astroid.points;
		astroid.points = nullptr;
		
		return *this;
	}

	Astroid::~Astroid()
	{
		delete[] points;
	}

	std::pair<Line, Line>* Astroid::getNormalAndTangent(int x, int y) const
	{
		if(std::abs(std::pow(std::cbrt((x-center.x)/radius), 2.)+std::pow(std::cbrt((y-center.y)/radius), 2.)-1.) > 0.04)
			return nullptr;

		double nX = x - center.x;
		double nY = y - center.y;
		
		const double k1 = -std::cbrt(nY/nX);
		const double b1 = std::pow(std::pow(radius, 2./3.)-std::pow(nX, 2./3.), 3./2.)+std::cbrt(nY/nX)*nX;

		const double k2 = -1/k1;
		const double b2 = nX/k1+nY;
		
		Line tangent(Vector3f(-1000, k1*(-1000.)+b1,0)+center, Vector3f(1000, k1*1000. + b1,0)+center);
		Line normal(Vector3f(-1000, k2*(-1000.)+b2,0)+center, Vector3f(1000, k2*1000. + b2,0)+center);

		return new std::pair<Line, Line>(normal, tangent);
	}

	void Astroid::setCenter(const Vector3f& center)
	{
		this->center = center;
		this->createPoints();
	}

	Vector3f Astroid::getCenter() const
	{
		return center;
	}

	bool Astroid::setRadius(float radius)
	{
		if(radius <= 0) 
			return false;
		
		this->radius = radius;
		this->createPoints();
		return true;
	}

	float Astroid::getRadius() const
	{
		return radius;
	}

	float Astroid::getA() const
	{
		return a;
	}

	bool Astroid::setA(float v)
	{
		a=v;
		this->createPoints();
		return true;
	}

	float Astroid::getB() const
	{
		return b;
	}

	bool Astroid::setB(float v)
	{
		b=v;
		this->createPoints();
		return true;
	}

	float Astroid::getArea() const
	{
		const auto PI = 3.14159265358979323846f;
		return (3.f/8.f) * PI * radius * radius;
	}

	float Astroid::getRadiusOfCurvature(float t) const
	{
		return (3.f/2.f)*radius*std::sin(2*t);
	}

	float Astroid::getArcLength() const
	{
		return 6*radius;
	}

	void Astroid::setPointCount(uint16_t pointCount)
	{
		this->pointCount = pointCount;

		delete[] points;
		
		points = new Vector3f[pointCount];
		this->createPoints();
	}

	uint16_t Astroid::getPointCount() const
	{
		return pointCount;
	}

	void Astroid::combineTransform(Transform& transform)
	{
		this->transform.combine(transform);
		this->animatedMovePoints();
	}

	void Astroid::draw() const
	{
		Vector3f secondPoint = points[0];
		
		for (uint_fast16_t i = 1; i < pointCount; i++)
		{
			const auto firstPoint = secondPoint;
			secondPoint = points[i];
			Brush::getInstance().drawLine(firstPoint, secondPoint);
		}
	}

	void Astroid::createPoints() const
	{
		for (uint_fast16_t i = 0; i < pointCount; i++)
		{
			points[i] = transform.multiply(this->getPoint(i));
		}
	}

	void Astroid::animatedMovePoints() const
	{
		for (uint_fast16_t i = 0; i < pointCount; i++)
		{
			PointsAnimator::getInstance().animatePoint(&points[i], transform.multiply(this->getPoint(i)));
		}
	}
	
	Vector3f Astroid::getPoint(uint16_t index) const
	{
		const auto PI = 3.14159265358979323846;

		const auto angle = static_cast<float>(PI * (2. * static_cast<double>(index) / (pointCount-1ui16)) - 0.5);
		const auto x = a*std::pow(std::cos(angle), 3) * radius + center.x;
		const auto y = b*std::pow(std::sin(angle), 3) * radius + center.y;

	    return Vector3f(x, y, 0);
	}
}
