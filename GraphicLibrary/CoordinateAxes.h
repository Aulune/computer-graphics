﻿#pragma once
#include "Object.h"
#include "Brush.h"
#include "Line.h"

namespace GraphicsLibrary
{
	class CoordinateAxes final : public Object
	{
	private:
		const float offset = 10;
		
		Vector3f zeroCoordinate;
		Vector3f tr_zeroCoordinate;
		Vector3f xTextCoordinate;
		Vector3f yTextCoordinate;
		
		float width;
		float height;

		Vector3f xAxis[4];
		Vector3f yAxis[4];

		Line lines[6];
	public:
		CoordinateAxes(const Vector3f& zeroCoordinate, float width, float height);

		[[nodiscard]] Vector3f getZeroCoordinate() const;
		
		void combineTransform(Transform& transform) override;
		
		void draw() const override;
	};
}

