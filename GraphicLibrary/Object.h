#pragma once
#include "Transform.h"

namespace GraphicsLibrary
{
	class Object
	{
	public:
		virtual ~Object() = default;
		
		virtual void combineTransform(Transform& transform) = 0;
		
		virtual void draw() const = 0;
	};
}
