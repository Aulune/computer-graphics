#include "Color.h"

namespace GraphicsLibrary
{
	Color Color::Black(0, 0, 0);
	Color Color::White(255, 255, 255);
	Color Color::LightGray(0, 0, 0, 30);
	Color Color::Red(255, 0, 0);
	Color Color::Green(0, 255, 0);
	Color Color::Blue(0, 0, 255);
}