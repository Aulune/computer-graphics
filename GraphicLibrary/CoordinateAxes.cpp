﻿#include "CoordinateAxes.h"

namespace GraphicsLibrary
{
	CoordinateAxes::CoordinateAxes(const Vector3f& zeroCoordinate, float width, float height)
		: zeroCoordinate(zeroCoordinate), tr_zeroCoordinate(zeroCoordinate), width(width), height(height)
	{
		xAxis[0].y = zeroCoordinate.y;
		yAxis[0].x = zeroCoordinate.x;

		xAxis[1].x = width;
		xAxis[1].y = zeroCoordinate.y;

		yAxis[1].y = height;
		yAxis[1].x = zeroCoordinate.x;

		xAxis[2].x = width - offset;
		xAxis[2].y = zeroCoordinate.y - offset / 2;
		xAxis[3].x = width - offset;
		xAxis[3].y = zeroCoordinate.y + offset / 2;

		yAxis[2].x = zeroCoordinate.x - offset / 2;
		yAxis[2].y = height - offset;
		yAxis[3].x = zeroCoordinate.x + offset / 2;
		yAxis[3].y = height - offset;

		xTextCoordinate = Vector3f(xAxis[1].x - offset, xAxis[1].y, 0);
		yTextCoordinate = Vector3f(yAxis[1].x + offset, yAxis[1].y - offset * 2, 0);

		lines[0].setPoints(xAxis[0], xAxis[1]);
		lines[1].setPoints(xAxis[1], xAxis[2]);
		lines[2].setPoints(xAxis[1], xAxis[3]);

		lines[3].setPoints(yAxis[0], yAxis[1]);
		lines[4].setPoints(yAxis[1], yAxis[2]);
		lines[5].setPoints(yAxis[1], yAxis[3]);
	}

	Vector3f CoordinateAxes::getZeroCoordinate() const
	{
		return zeroCoordinate;
	}

	void CoordinateAxes::combineTransform(Transform& transform)
	{
		tr_zeroCoordinate = transform.multiply(tr_zeroCoordinate);
		xTextCoordinate = transform.multiply(xTextCoordinate);
		yTextCoordinate = transform.multiply(yTextCoordinate);
		for (auto& line : lines)
		{
			line.combineTransform(transform);
		}
	}

	void CoordinateAxes::draw() const
	{
		Brush::getInstance().setColor(Color::Blue);

		for (const auto& line : lines)
		{
			line.draw();
		}

		Brush::getInstance().drawText("x", xTextCoordinate.x, xTextCoordinate.y, 15, Color::Blue);
		Brush::getInstance().drawText("y", yTextCoordinate.x, yTextCoordinate.y, 15, Color::Blue);

		Brush::getInstance().drawText("0", tr_zeroCoordinate.x + 5, tr_zeroCoordinate.y, 15, Color::Blue);
	}
}
