﻿#pragma once
#include "Vector.h"
#include "Transform.h"

namespace GraphicsLibrary
{
	class AnimatedPoint
	{
	private:
		Vector3f*const point;
		Vector3f start;
		Vector3f end;
		Vector3f offset;

		bool _isActive = false;

		const float speed = 1;
		
	public:
		explicit AnimatedPoint(Vector3f*const point);
		AnimatedPoint(Vector3f*const point, const Vector3f& endPoint);

		void transform(const Vector3f& endPoint);

		[[nodiscard]] bool isActive() const;

		void update(const float deltaTime);
	};
}
