﻿#pragma once
#include <vector>

#include "CoordinateAxes.h"
#include "Line.h"

namespace GraphicsLibrary
{
	class Grid final : public Object
	{
	private:
		std::vector<Line*> lines;

		CoordinateAxes axes;
		
		int cellSize;
	public:

		Grid(int width, int height, int cellSize);
		~Grid() override;

		[[nodiscard]] int getCellSize() const;

		[[nodiscard]] Vector3f getZeroCoordinate() const;
		
		void combineTransform(Transform& transform) override;
		void draw() const override;
	};
}

