﻿#pragma once
#include <list>

#include "AnimatedPoint.h"
#include "Vector.h"

namespace GraphicsLibrary
{
	class PointsAnimator final
	{
	private:
		std::list<AnimatedPoint> points;
		
	public:
		static PointsAnimator& getInstance()
		{
			static PointsAnimator instance;
			return instance;
		}

		void animatePoint(Vector3f*const point, const Vector3f& endPoint);

		void update(const float deltaTime);
		
	private:
		PointsAnimator() = default;
		PointsAnimator(const PointsAnimator&) = default;
		PointsAnimator(PointsAnimator&&) = default;
	};
}
