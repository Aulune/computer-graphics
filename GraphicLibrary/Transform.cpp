#include "Transform.h"

namespace GraphicsLibrary
{
	Transform::Transform() noexcept
	{
		// make diagonal 1 matrix
		matrix[0] = 1;
		matrix[5] = 1;
		matrix[10] = 1;
		matrix[15] = 1;
	}

	Transform::Transform(const Transform& transform) noexcept
	{
		this->setMatrix(transform.matrix);
	}

	Transform& Transform::operator=(const Transform& transform) noexcept
	{
		if (this == &transform) return *this;

		this->setMatrix(transform.matrix);
		return *this;
	}

	Transform::Transform(Transform&& transform) noexcept
	{
		this->setMatrix(transform.matrix);
	}

	Transform& Transform::operator=(Transform&& transform) noexcept
	{
		if (this == &transform) return *this;

		this->setMatrix(transform.matrix);
		return *this;
	}

	void Transform::scale(const Vector3f& scale)
	{
		this->scale(scale.x, scale.y, scale.z);
	}

	void Transform::move(const Vector3f& vector)
	{
		this->move(vector.x, vector.y, vector.z);
	}

	void Transform::rotate(const Vector3f& angle)
	{
		this->rotate(angle.x, angle.y, angle.z);
	}

	void Transform::rotate(const Vector3f& angle, const Vector3f& center)
	{
		// TODO: check why in theory it has to be in reverse order
		this->move(center);
		this->rotate(angle);
		this->move(-center);
	}

	void Transform::scale(float x, float y, float z)
	{
		Transform scale;
		scale.matrix[0] = x;
		scale.matrix[5] = y;
		scale.matrix[10] = z;

		this->combine(scale);
	}

	void Transform::move(float x, float y, float z)
	{
		Transform move;
		move.matrix[12] = x;
		move.matrix[13] = y;
		move.matrix[14] = z;

		this->combine(move);
	}

	void Transform::rotate(float x, float y, float z)
	{
		const double PI = 3.14159265358979323846;

		x *= PI / 180;
		y *= PI / 180;
		z *= PI / 180;


		Transform rotateX;
		rotateX.matrix[5] = std::cos(x);
		rotateX.matrix[10] = std::cos(x);
		rotateX.matrix[6] = std::sin(x);
		rotateX.matrix[9] = -std::sin(x);

		this->combine(rotateX);

		Transform rotateY;
		rotateY.matrix[0] = std::cos(y);
		rotateY.matrix[10] = std::cos(y);
		rotateY.matrix[8] = std::sin(y);
		rotateY.matrix[2] = -std::sin(y);

		this->combine(rotateY);


		Transform rotateZ;
		rotateZ.matrix[0] = std::cos(z);
		rotateZ.matrix[5] = std::cos(z);
		rotateZ.matrix[4] = -std::sin(z);
		rotateZ.matrix[1] = std::sin(z);

		this->combine(rotateZ);
	}

	void Transform::affine(const Vector3f& r0, const Vector3f& rX, const Vector3f& rY)
	{
		Transform affine;
		affine.matrix[0] = rX.x;
		affine.matrix[1] = rX.y;
		
		affine.matrix[4] = rY.x;
		affine.matrix[5] = rY.y;

		affine.matrix[12] = r0.x;
		affine.matrix[13] = r0.y;
		this->combine(affine);
	}

	void Transform::homography(const Vector3f& r0, const Vector3f& rX, const Vector3f& rY, const Vector3f& w)
	{
		Transform homography;
		homography.matrix[0] = rX.x * w.x;
		homography.matrix[1] = rY.x * w.x;
		homography.matrix[3] = w.x;
		
		homography.matrix[4] = rY.y * w.y;
		homography.matrix[5] = rY.y * w.y;
		homography.matrix[7] = w.y;

		homography.matrix[10] = 1;
		homography.matrix[11] = 1;
		
		homography.matrix[12] = r0.x * w.z;
		homography.matrix[13] = r0.y * w.z;
		homography.matrix[15] = w.z;

		this->combine(homography);
	}

	Vector3f Transform::multiply(const Vector3f& point) const
	{
		return this->multiply(point.x, point.y, point.z);
	}

	Vector3f Transform::multiply(float x, float y, float z) const
	{
		Vector3f result;
		
		// c14 = a11 * b14 + a12 * b24 + a13 * b34 + a14 * b44
		const float ws = x * matrix[3] + y * matrix[7] + z * matrix[11] + matrix[15];
		// c11 = a11 * b11 + a12 * b21 + a13 * b31 + a14 * b41
		result.x = (x * matrix[0] + y * matrix[4] + z * matrix[8] + matrix[12]) / ws;
		// c12 = a11 * b12 + a12 * b22 + a13 * b32 + a14 * b42
		result.y = (x * matrix[1] + y * matrix[5] + z * matrix[9] + matrix[13]) / ws;
		// c13 = a11 * b13 + a12 * b23 + a13 * b33 + a14 * b43
		result.z = (x * matrix[2] + y * matrix[6] + z * matrix[10] + matrix[14]) / ws;
		return result;
	}

	Transform& Transform::combine(const Transform& transform)
	{
		return this->combine(transform.matrix);
	}

#ifdef _DEBUG
	void Transform::print() const
	{
		for (int x = 0; x < 4; x++)
		{
			for (int y = 0; y < 4; y++)
			{
				std::cout << this->matrix[x*4 + y];
				std::cout << " | ";
			}
			std::cout << std::endl;
		}
		std::cout << std::endl;
	}
#endif

	Transform& Transform::combine(float const* const transform)
	{
		float result[16];

		for (int x = 0; x < 4; x++)
		{
			for (int y = 0; y < 4; y++)
			{
				float sum = this->matrix[x] * transform[4 * y];
				for (int m = 1; m < 4; m++)
				{
					sum += this->matrix[x + 4 * m] * transform[m + 4 * y];
				}
				result[x + 4 * y] = sum;
			}
		}

		this->setMatrix(result);
		return *this;
	}

	void Transform::setMatrix(float const* const matrix)
	{
		std::memcpy(this->matrix, matrix, 16 * sizeof(float));
	}
}
