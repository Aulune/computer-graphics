﻿#include "Circle.h"

namespace GraphicsLibrary
{
	Circle::Circle()  // NOLINT(cppcoreguidelines-pro-type-member-init)
	{
		this->setPointCount(30);
	}

	Circle::Circle(const Vector3f& center, float radius, uint16_t pointCount)  // NOLINT(cppcoreguidelines-pro-type-member-init)
		:center(center), radius(radius)
	{
		this->setPointCount(pointCount);
	}

	Circle::Circle(const Circle& circle)  // NOLINT(cppcoreguidelines-pro-type-member-init)
	{
		center = circle.center;
		radius = circle.radius;
		transform = circle.transform;

		this->setPointCount(circle.pointCount);
	}

	Circle& Circle::operator=(const Circle& circle)
	{
		if(this == &circle) return *this;
		
		center = circle.center;
		radius = circle.radius;
		transform = circle.transform;

		this->setPointCount(circle.pointCount);
		return *this;
	}

	Circle::Circle(Circle&& circle) noexcept
	{
		center = circle.center;
		radius = circle.radius;
		transform = std::move(circle.transform);
		pointCount = circle.pointCount;
		points = circle.points;

		circle.points = nullptr;
	}

	Circle& Circle::operator=(Circle&& circle) noexcept
	{
		if(this == &circle) return *this;
		
		center = circle.center;
		radius = circle.radius;
		transform = std::move(circle.transform);
		pointCount = circle.pointCount;

		points = circle.points;
		circle.points = nullptr;
		
		return *this;
	}

	Circle::~Circle()
	{
		delete[] points;
	}

	void Circle::setCenter(const Vector3f& center)
	{
		this->center = center;
		this->createPoints();
	}

	Vector3f Circle::getCenter() const
	{
		return center;
	}

	void Circle::setRadius(const float radius)
	{
		this->radius = radius;
		this->createPoints();
	}

	float Circle::getRadius() const
	{
		return radius;
	}

	void Circle::setPointCount(uint16_t pointCount)
	{
		this->pointCount = pointCount;

		delete[] points;
		
		points = new Vector3f[pointCount];
		this->createPoints();
	}

	uint16_t Circle::getPointCount() const
	{
		return pointCount;
	}

	void Circle::combineTransform(Transform& transform)
	{
		this->transform.combine(transform);
		this->createPoints();
	}

	void Circle::draw() const
	{
		for (uint_fast16_t i = 1; i < pointCount; i++)
		{
			Brush::getInstance().drawLine(points[i-1], points[i]);
		}
	}

	void Circle::createPoints() const
	{
		for (uint_fast16_t i = 0; i < pointCount; i++)
		{
			points[i] = transform.multiply(this->getPoint(i));
		}
	}

	Vector3f Circle::getPoint(const uint16_t index) const
	{
		const auto PI = 3.14159265358979323846;

		const auto angle = static_cast<float>(PI * (2. * static_cast<double>(index) / (pointCount-1ui16)) - 0.5);
		const auto x = std::cos(angle) * radius + center.x;
		const auto y = std::sin(angle) * radius + center.y;

	    return Vector3f(x, y, 0);
	}
}
