﻿#include "Brush.h"

namespace GraphicsLibrary
{
	Brush& Brush::getInstance()
	{
		static Brush instance;
		return instance;
	}

	void Brush::setDrawPointCallback(std::function<void(float, float)>&& func)
	{
		drawPointCallback = std::move(func);
	}
	
	void Brush::setDrawLineCallback(std::function<void(float, float, float, float)>&& func)
	{
		drawLineCallback = std::move(func);
	}

	void Brush::setChangeColorCallback(std::function<void(uint8_t, uint8_t, uint8_t, uint8_t)>&& func)
	{
		changeColorCallback = std::move(func);
	}

	void Brush::drawPoint(float x, float y) const
	{
		drawPointCallback(x, y);
	}

	void Brush::drawPoint(Vector3f point) const
	{
		drawPointCallback(point.x, point.y);
	}

	void Brush::drawLine(float x1, float y1, float x2, float y2) const
	{
		drawLineCallback(x1, y1, x2, y2);
	}

	void Brush::drawLine(Vector3f p1, Vector3f p2) const
	{
		drawLineCallback(p1.x, p1.y, p2.x, p2.y);
	}

	void Brush::setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a) const
	{
		changeColorCallback(r, g, b, a);
	}

	void Brush::setColor(const Color& color) const
	{
		changeColorCallback(color.r, color.g, color.b, color.a);
	}

	void Brush::setDrawTextCallback(std::function<bool(const char*, float, float, int, Color)>&& func)
	{
		drawTextCallback = std::move(func);
	}

	void Brush::drawText(const char* text, float x, float y, int size, Color color) const
	{
		if(!drawTextCallback(text, x, y, size, color))
			throw std::exception("Error drawing text");
	}

	void Brush::drawText(const char* text, Vector3f position, int size, Color color) const
	{
		if(!drawTextCallback(text, position.x, position.y, size, color))
			throw std::exception("Error drawing text");
	}
}
