#include "MyVariant.h"

namespace GraphicsLibrary
{
	MyVariant::MyVariant(Vector3f zeroCoordinate)
	{
		Z = zeroCoordinate - Vector3f(0, cell * 5, 0);
		O1 = zeroCoordinate;

		for (int i = 0; i < 9; ++i)
		{
			rightPoints[i] = leftPoints[i];
			rightPoints[i].y = -rightPoints[i].y;
		}
		this->recreateLines();
		
		arcs[1].setCenter(O1);
		arcs[1].setRadius(cell);
		arcs[1].setPhi1(0);
		arcs[1].setPhi2(180);
		this->conjugation(Z, lines[7].getSecondPoint(), cell / 3);

		circle.setCenter(O1);
		circle.setRadius(cell / 2);
	}

	void MyVariant::combineTransform(Transform& transform)
	{
		for (auto& line : additionalLines)
		{
			line.combineTransform(transform);
		}
		for (auto& line : lines)
		{
			line.combineTransform(transform);
		}
		for (auto& arc : arcs)
		{
			arc.combineTransform(transform);
		}
		circle.combineTransform(transform);
	}

	void MyVariant::draw() const
	{
		Brush::getInstance().setColor(Color::Red);
		for (const auto& line : additionalLines)
		{
			line.draw();
		}

		Brush::getInstance().setColor(Color::Black);
		for (const auto& line : lines)
		{
			line.draw();
		}
		for (const auto& arc : arcs)
		{
			arc.draw();
		}
		circle.draw();
	}

	bool MyVariant::setR1(float radius)
	{
		if (arcs[1].getRadius() <= radius)
			return false;

		circle.setRadius(radius);
		return true;
	}
	float MyVariant::getR1() const
	{
		return circle.getRadius();
	}

	bool MyVariant::setR2(float radius)
	{
		if (radius >= getL2() || radius <= getR1())
			return false;

		arcs[1].setRadius(radius);

		auto point = lines[0].getFirstPoint();
		point.x = O1.x - radius;
		lines[0].setFirstPoint(point);

		point = lines[9].getFirstPoint();
		point.x = O1.x + radius;
		lines[9].setFirstPoint(point);

		return true;
	}
	float MyVariant::getR2() const
	{
		return arcs[1].getRadius();
	}

	bool MyVariant::setR3(float radius)
	{
		if (O1.x - lines[8].getSecondPoint().x <= radius)
			return false;

		this->conjugation(Z, lines[8].getSecondPoint(), radius);
		return true;
	}
	float MyVariant::getR3() const
	{
		return arcs[0].getRadius();
	}

	bool MyVariant::setL1(float size)
	{
		if (size >= getL2())
			return false;
		if (size <= getL6())
			return false;

		leftPoints[7].x = size;
		leftPoints[6].x = size;

		rightPoints[7].x = size;
		rightPoints[6].x = size;

		this->recreateLines();

		return true;
	}
	float MyVariant::getL1() const
	{
		return std::abs(O1.x - lines[7].getFirstPoint().x);
	}

	bool MyVariant::setL2(float size)
	{
		if(size <= getL1())
			return false;
		if(size <= getL5())
			return false;
		if(size <= getR2())
			return false;

		leftPoints[1].x = size;
		leftPoints[2].x = size;

		rightPoints[1].x = size;
		rightPoints[2].x = size;
		
		this->recreateLines();

		return true;
	}
	float MyVariant::getL2() const
	{
		return std::abs(O1.x - lines[0].getSecondPoint().x);
	}

	bool MyVariant::setL3(float size)
	{
		leftPoints[2].y = size;
		rightPoints[2].y = -size;
		
		this->recreateLines();

		return true;
	}
	float MyVariant::getL3() const
	{
		return std::abs(lines[1].getSecondPoint().y - lines[1].getFirstPoint().y);
	}

	bool MyVariant::setL4(float size)
	{
		if (size <= getL3())
			return false;

		leftPoints[3].y = size;
		leftPoints[4].y = size;
		
		rightPoints[3].y = -size;
		rightPoints[4].y = -size;
		
		this->recreateLines();

		return true;
	}
	float MyVariant::getL4() const
	{
		return std::abs(O1.y - lines[3].getFirstPoint().y);
	}

	bool MyVariant::setL5(float size)
	{
		if (size >= getL2() || size <= getL6())
			return false;

		leftPoints[3].x = size;
		rightPoints[3].x = size;
		
		this->recreateLines();

		return true;
	}
	float MyVariant::getL5() const
	{
		return std::abs(O1.x - lines[3].getFirstPoint().x);
	}

	bool MyVariant::setL6(float size)
	{
		leftPoints[4].x = size;
		leftPoints[5].x = size;
		
		rightPoints[4].x = size;
		rightPoints[5].x = size;
		
		this->recreateLines();

		return true;
	}
	float MyVariant::getL6() const
	{
		return std::abs(O1.x - lines[3].getSecondPoint().x);
	}

	bool MyVariant::setL7(float size)
	{
		if(size <= getR3())
			return false;
		
		leftPoints[8].x = size;
		rightPoints[8].x = size;
		
		this->recreateLines();

		this->conjugation(Z, lines[7].getSecondPoint(), arcs[0].getRadius());

		return true;
	}
	float MyVariant::getL7() const
	{
		return std::abs(O1.x - lines[7].getSecondPoint().x);
	}

	bool MyVariant::setL8(float size)
	{
		if (size >= getL4())
			return false;
		
		leftPoints[5].y = size;
		leftPoints[6].y = size;
		rightPoints[5].y = -size;
		rightPoints[6].y = -size;
		
		this->recreateLines();

		return true;
	}
	float MyVariant::getL8() const
	{
		return std::abs(O1.y - lines[4].getSecondPoint().y);
	}

	bool MyVariant::setL9(float size)
	{
		Z.y = O1.y - size;
		this->conjugation(Z, lines[8].getSecondPoint(), arcs[0].getRadius());
		return true;
	}
	float MyVariant::getL9() const
	{
		return std::abs(Z.y - O1.y);
	}

	void MyVariant::recreateLines()
	{
		for (int i = 0; i < 8; ++i)
		{
			lines[i].setPoints(O1 - leftPoints[i], O1 - leftPoints[i+1]);	
		}
		
		for (int i = 0; i < 8; ++i)
		{
			lines[9+i].setPoints(O1 + rightPoints[i], O1 + rightPoints[i+1]);
		}
	}

	void MyVariant::conjugation(const Vector3f& Z, const Vector3f& Q1, float R)
	{
		const Vector3f Q2(2. * Z.x - Q1.x, Q1.y, Q1.z);

		auto movePoint = [](const Vector3f& P, const Vector3f& Z, const Vector3f& Q, float dist)-> Vector3f
		{
			const Vector3f n(-(Q.y - Z.y), Q.x - Z.x, 0);
			float len = std::sqrt(n.x * n.x + n.y * n.y);
			return Vector3f(P.x + dist * n.x / len, P.y + dist * n.y / len, 0);
		};

		Vector3f Z2h = movePoint(Z, Z, Q2, R);
		Vector3f Q2h = movePoint(Q2, Z, Q2, R);

		Vector3f Z1h = movePoint(Z, Z, Q1, -R);
		Vector3f Q1h = movePoint(Q1, Z, Q1, -R);

		Vector3f O = intersection(getLine(Z1h, Q1h), getLine(Z2h, Q2h));

		Vector3f I1 = movePoint(O, Z, Q1, R);
		Vector3f I2 = movePoint(O, Z, Q2, -R);

		arcs[0].setCenter(O);
		arcs[0].setRadius(R);

		arcs[0].setPhi2(I1);
		arcs[0].setPhi1(I2);

		lines[8].setPoints(I1, Q1);
		lines[17].setPoints(I2, Q2);

		additionalLines[0].setPoints(Z, Q1);
		additionalLines[1].setPoints(Z, Q2);
	}

	std::pair<float, float> MyVariant::getLine(const Vector3f& A, const Vector3f& B)
	{
		std::pair<float, float> line;
		line.first = (A.y - B.y) / (A.x - B.x);
		line.second = A.y - line.first * A.x;
		return line;
	}
	Vector3f MyVariant::intersection(const std::pair<float, float>& a, const std::pair<float, float>& b)
	{
		Vector3f point;
		point.x = (b.second - a.second) / (a.first - b.first);
		point.y = point.x * a.first + a.second;
		return point;
	}

}
