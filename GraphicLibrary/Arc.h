﻿#pragma once
#include "Object.h"
#include "Brush.h"

namespace GraphicsLibrary
{
	class Arc final : public Object
	{
	private:
		Vector3f center;
		
		float radius{};

		float phi1{};
		float phi2{};
		
		uint16_t pointCount;

		Transform transform;

		Vector3f* points = nullptr;
		
	public:
		Arc();
		
		Arc(const Vector3f& center, float radius,
			float phi1, float phi2,
			uint16_t pointCount = 30);

		Arc(const Arc& arc);
		Arc& operator=(const Arc& arc);
		
		Arc(Arc&& arc) noexcept;
		Arc& operator=(Arc&& arc) noexcept;
		
		~Arc() override;

		void setCenter(const Vector3f& center);
		[[nodiscard]] Vector3f getCenter() const;
		
		void setRadius(float radius);
		[[nodiscard]] float getRadius() const;

		void setPhi1(float phi1);
		void setPhi1(const Vector3f& point);
		[[nodiscard]] float getPhi1() const;

		void setPhi2(float phi2);
		void setPhi2(const Vector3f& point);
		[[nodiscard]] float getPhi2() const;
		
		void setPointCount(uint16_t pointCount);
		[[nodiscard]] uint16_t getPointCount() const;
		
		void combineTransform(Transform& transform) override;
		void draw() const override;

	private:
		void createPoints() const;
	};
}
